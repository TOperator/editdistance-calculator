/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package editdistance;

import javax.swing.UIManager;

/**
 *
 * @author Tim
 */
public class EditDistance {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Set os layout
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            //Doesn't matter
        }

        
        EditDistanceJFrame frame = new EditDistanceJFrame();
        frame.setVisible(true);
    }
    
}
